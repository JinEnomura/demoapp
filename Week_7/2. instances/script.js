function createMeteorite(weight, density, composition) {
    var meteorite = {};
    this.weight = weight;
    this.density = density;
    this.composition = composition;
}

var meteorite1 = new createMeteorite(100, 10, ["Iron", "gold"]);
//var meteorite2 = new createMeteorite(500, 100);
//var meteorite3 = new createMeteorite(10, 1000);
//var meteorite4 = new createMeteorite(500, 50);
//var meteorite5 = new createMeteorite(100, 80);

console.log(meteorite1);
//console.log(meteorite2);
//console.log(meteorite3);
//console.log(meteorite4);
//console.log(meteorite5);

class meteoriteClass {
    constructor(weight, density, composition) {
        this.weight = weight;
        this.density = density;
        this.composition = composition;
    }
}

var meteoriteClass1 = new meteoriteClass(100,2000, ["iron","silver"]);
//var meteoriteClass2 = new meteoriteClass(10,500);
//var meteoriteClass3 = new meteoriteClass(800,10);
//var meteoriteClass4 = new meteoriteClass(249,14);
//var meteoriteClass5 = new meteoriteClass(500,500);

console.log(meteoriteClass1);
//console.log(meteoriteClass2);
//console.log(meteoriteClass3);
//console.log(meteoriteClass4);
console.log(meteoriteClass5);