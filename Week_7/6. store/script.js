
class Customer {
    constructor(name, balance, debts) {
        this.name = name;
        this.balance = balance;
        this.debts = debts;
    }
    buy(price) {
        this.balance -= price;
        return;
    }
    createDebt(value) {
        this.debts += value;
        return;
    }
    robBank() {
        this.balance += 10000;
        return;
    }
}


class NormalCustomer extends Customer { };
class CustomerVip extends Customer { };


var customer1 = new NormalCustomer();
var customer2 = new NormalCustomer();
var vipCustomer1 = new CustomerVip();
var vipCustomer2 = new CustomerVip();


var customers = [vipCustomer1, vipCustomer2, customer1, customer2];

class Product {
    constructor(name, price, category, units) {
        this.name = name;
        this.price = price;
        this.category = category;
        this.units = units;
    }
}

class NormalProduct extends Product { };
class VipProduct extends Product { };


for (let i = 0; i < 4; i++) {
    var ask1 = prompt("Name of the customer? 2 VIP clients first and then 2 normal clients");
    var ask2 = prompt("whats the balance of this client?");

    customers[i].name = ask1;
    customers[i].balance = ask2;
    customers[i].debt = 0;
}


var products = [];


productCount = function () {
    var ask4 = parseInt(prompt("How many products we have today?"));

    for (let p = 0; p < ask4; p++) {
        var ask5 = prompt("tell us the product name");
        var ask6 = prompt("tell us the product price");
        var ask7 = prompt("tell us the product category");
        var ask8 = Number(prompt("tell us how many units this product has"));
        var ask9 = prompt("this product is 'vip' or 'normal'?");

        if (ask9 == 'Normal') {
            let product = new NormalProduct(ask5, ask6, ask7, ask8);
            products.push(product);
        }
        if (ask9 == 'Vip') {
            let product = new VipProduct(ask5, ask6, ask7, ask8);
            products.push(product);
        }
    }
    alert("The store is open !");
}

productCount();

askClient = function () {

    var ask10 = prompt("Welcome to the store, what's your name?");
    if (ask10 == "store_closed*") {
        return 1;
    }

    alert("list of products:");
    for (let i = 0; i < products.length; i++) {
        alert(products[i].name + " " + products[i].price + " " + products[i].category + " " + products[i].units);
    }

    var ask11 = prompt("Which product do you want to buy?");

    var hasProduct = false;
    var hasStock = false;
    var product = null;
    var clienteActual;

    for (var produto in products) {
        if (products[produto].name == ask11) {
            var isProductVip = products[produto] instanceof VipProduct;
            hasProduct = true;

            if (products[produto].units >= 1) {
                hasStock = true;
            }

            product = products[produto];
        }
    }

    for (var cliente in customers) {
        if (customers[cliente].name == ask10) {
            var isCustomerVip = customers[cliente] instanceof CustomerVip;

            clienteActual = customers[cliente]
        }
    }


    if (isProductVip && !isCustomerVip) {
        alert("You are not on the VIP list, sorry")
    }
    else if (!hasProduct) {
        alert("We don't have that")
    }
    else if (!hasStock) {
        alert("We ran out of " + ask11 + ", sorry");
    }

    else {
        if (product.price > clienteActual.balance) {
            alert("Your credit card does not work, you don't have money");
        }
        else {
            clienteActual.buy(product.price);
            product.units -= 1;
            alert("Thank you for your purchase, bye");
        }
    }
    return 0;
}

var exit = 0;
while (!exit) {
    exit = askClient();
}



