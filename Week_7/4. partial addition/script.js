
function addArg(a,b,c,d,e) {
    return a + b + c + d + e;
}


var result1 = addArg.bind(null, 1);
var result2 = result1.bind(null, 2);
var result3 = result2.bind(null, 3);
var result4 = result3.bind(null, 4);
var result5 = result4.bind(null, 5);


console.log(result5());
