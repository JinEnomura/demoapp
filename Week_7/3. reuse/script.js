var greet = function (age, gender, name) {
    this.age = age;
    this.gender = gender;
    this.name = name;

    if (age >= 40 && gender == "male") {
        console.log("Hello Mr." + name);
    }
    if (age >= 40 && gender == "female") {
        console.log("Hello Ms." + name);
    }
    if (age < 40 && gender == "male") {
        console.log("Hey " + name + " my boy");
    }
    if (age < 40 && gender == "female") {
        console.log("Hey " + name + " my girl");
    }
}

let greetAnAdultMale = greet.bind(null, 48, "male");
let greetAYoungster = greet.bind(null, 30);

greetAnAdultMale("Joao");
greetAYoungster("male", "Pedro");
greetAYoungster("female", "Catarina");