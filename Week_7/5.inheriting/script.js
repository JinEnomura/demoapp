// class spaceObject {
//     constructor(weight, kmTraveled, timeElapsed) {
//         this.weight = weight;
//         this.kmTraveled = kmTraveled;
//         this.timeElapsed = timeElapsed;
//     }
//     getVelocity() {
//         return this.kmTraveled / this.timeElapsed;
//     }
// };

// let planet = new spaceObject(100,50,100);
// let meteorite = new spaceObject(200,2000,0.3);

// console.log(planet);
// console.log(meteorite);

// console.log(planet.getVelocity());
// console.log(meteorite.getVelocity());



var spaceObject = function (weight, kmTraveled, timeElapsed) {
    this.weight = weight;
    this.kmTraveled = kmTraveled;
    this.timeElapsed = timeElapsed;
}

spaceObject.prototype.getVelocity = function() {
        return this.kmTraveled / this.timeElapsed + "Km/h";
}


let planet = new spaceObject(100, 50, 100);
let meteorite = new spaceObject(200,2000,0.3);

console.log(planet.getVelocity());
console.log(meteorite.getVelocity());