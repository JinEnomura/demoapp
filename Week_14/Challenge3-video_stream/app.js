var express = require('express');
var path = require('path');
var fs = require('fs');
var app = express();


var port = 3000;

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));

})

app.get('/video', function (req, res) {
    res.writeHead(200, {
        'Content-type': 'Video/mp4'
    });
    var rs = fs.createReadStream("sample.mp4");
    rs.pipe(res)
});

app.get('*', function (req, res) {
    res.send('404 not found', 404);
})

app.listen(port, function () {
    console.log('Server initiated on port: ' + port);
})