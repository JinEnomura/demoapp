var fs = require('fs');
var express = require('express');
var zipdir = require('zip-dir');

var app = express();

app.get('/', function (req, res) {
    console.log('Server started on port 3000');
    res.send('Welcome to the homepage');
})

app.get('/download/myphotos', function (req, res) {
    zipdir(__dirname + '/myphotos', {
        saveTo: __dirname + '/myzip.zip'
    }, function (err, buffer) {
        res.setHeader('Content-type', 'application/zip');

        let fileStream = fs.createReadStream(__dirname + '/myzip.zip', );
        fileStream.pipe(res);
    });
})

app.get('*', function (req, res) {
    res.send("404 Not Found", 404);
})

app.listen(3000)
console.log('yo dawgs, now listening to port 3000');