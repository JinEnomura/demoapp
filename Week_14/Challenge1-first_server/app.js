var express = require('express');
var app = express();
var array = [{
        title: "International Solutions Engineer",
        content: "digital",
        author: "Virgil",
        category: "Wednesday"
    },
    {
        title: "Forward Usability Producer",
        content: "online",
        author: "Laurence",
        category: "Wednesday"
    },
    {
        title: "Forward Factors Developer",
        content: "optical",
        author: "Fermin",
        category: "Sunday"
    },
    {
        title: "International Factors Coordinator",
        content: "primary",
        author: "Ethan",
        category: "Sunday"
    },
    {
        title: "Internal Marketing Facilitator",
        content: "multi-byte",
        author: "Ardith",
        category: "Saturday"
    }
];

app.get('/', function (req, res) {
    res.send('Welcome to the homepage')
})

port = 3000;

app.get('/posts', function (req, res) {
    res.send(array);
})

app.get('/posts/1', function (req, res) {
    res.send(array[0]);
})

app.get('/posts/2', function (req, res) {
    res.send(array[1]);
})

app.get('/posts/3', function (req, res) {
    res.send(array[2]);
})

app.get('/posts/4', function (req, res) {
    res.send(array[3]);
})

app.get('/posts/5', function (req, res) {
    res.send(array[4]);
})

app.get('*', function (req, res) {
    res.send('404 Not Found boi', 404);
})

app.listen(port, function () {
    console.log('server initiated on port: ' + port)
});