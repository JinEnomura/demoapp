var element = [];

askUser = function () {
    var askUserName = prompt("insert user name");
    if (askUserName != "terminate*") {

        var askUserAge = Number(prompt("insert user age"));
        var askUserJob = prompt("insert user's job");

        var pessoa = { name: askUserName, age: askUserAge, job: askUserJob };
        element.push(pessoa);
        return false;
    }
    else {
        return true;
    }
}

bubbleSort = function () {
    var swap = true;
    while (swap) {
        swap = false;
        for (let i = 0; i < element.length-1; i++) {
            if (element[i].age > element[i + 1].age) {
                var temporario = element[i];
                element[i] = element[i + 1];
                element[i + 1] = temporario;
                swap = true;
            }
        }
    }
}



var isTerminate = false;
while (!isTerminate) {
    isTerminate = askUser();
}

if (element.length < 1) {
    alert("You did not insert any person in the list")
} else {
    bubbleSort();
    var lista = "";
    for (let i = 0; i < element.length; i++) {
        lista += element[i].name + " - " + element[i].age;
        if (i != element.length - 1) {
            lista += ", ";
        }
    }
    alert("list: " + lista);
}
alert("Goodbye");