//TESTE Nº 1

let teste1 = 
['sign up',
'ze',
'teste@gmail.com',
'123',
'log in',
'teste@gmail.com',
'123',
'search',
'teste@gmail.com',
'log out',
'exit'];

//TESTE Nº 2

let teste2 = 
['sign up',
'ze',
'teste@gmail.com',
'123',
'log in',
'teste@gmail.com',
'12356',
'log in',
'teste@gmail.com',
'123567',
'log in',
'teste@gmail.com',
'1235622',
'log in',
'teste@gmail.com',
'123',
'log out',
'log in',
'a@hotmail.com',
'123',
'log in',
'a@hotmail.com',
'123',
'exit'];

//TESTE Nº 3

let teste3 = 
['sign up',
'ze',
'eddisruptgmaill.com',
'eddisrupt@.com',
'eddisrupt@coisas.',
'eddisrupt@',
'eddisrupt@gmail',
'eddisrupt',
'eddisruptb!@gmail',
'eddisrupt@@gmail',
'eddisrupt @gmail.com',
'eddisrupt@gma il.com',
'eddisrupt@gmail. com',
'ed!disrupt@gmail. com',
'eddisru!pt@gmail. com',
'edd!isrupt@gmail.com',
'eddisr!upt@gmail.com',
'e!!!!ddisrupt@gmail.com',
'eddi@srupt*gmail',
'eddisrupt?@gmail',
'@gmail.com',
'@',
' ',
'zeca@gmail.com',
'123',
'log out',
'log in',
' zeca@gmail.com',
'123',
'search',
' zeca@gmail.com',
'log out',
'sign up',
'ze',
'oi@hotmail.com ',
'111',
'log in',
'oi@hotmail.com ',
'111',
'search',
'oi@hotmail.com ',
'log out',
'sign up',
'ze',
'eddisrupt@hotmail.br',
'222',
'log in',
'eddisrupt@hotmail.br',
'222',
'search',
'eddisrupt@hotmail.br',
'log out',
'sign up',
'ze',
'eddisrupt@goodemail.xyz',
'333',
'log in',
'eddisrupt@goodemail.xyz',
'333',
'search',
'eddisrupt@goodemail.xyz',
'log out',
'sign up',
'joka',
' eddisrupt@hotmail.es',
'555',
'log in',
'eddisrupt@hotmail.es', 
'555',
'search',
'eddisrupt@hotmail.es',
'log out',
'exit'];

//TESTE Nº 4

let teste4 =
['asjhashj',
'Ola',
'sign up',
'zeca',
'zeca@topmail.br',
'123',
'sign up',
'ze',
'ze@zipmail.pt',
'abcd',
'log in',
'zeca@topmail.br',
'123',
'search',
'zeca@topmail.br',
'search',
'ze@zipmail.pt',
'follow',
'ze@zipmail.pt',
'search',
'zeca@topmail.br',
'search',
'ze@zipmail.pt',
'follow',
'shsh@gmail.com',
'log out',
'follow',
'log out',
'exit'];

//TESTE Nº 5

let teste5 = 
['search',
'sign up',
'joao',
'jonas_pistolas@fct.unl.pt',
'aaa',
'log in',
'jonas_pistolas@fct.unl.pt',
'aaa',
'log in',
'sign up',
'log out',
'sign up',
'bruno de carvalho',
'jonas_pistolas@fct.unl.pt',
'exit*',
'exit'];

let alert = console.log.bind(null);

let prompt = function(str){
    let teste = teste3;
    let answer = teste.shift();
    console.log(str);
    console.log(answer);
    return answer;
}


module.exports = {
    alert: alert,
    prompt: prompt
}