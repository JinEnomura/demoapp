const ask = [
    "input your option: Sign up, Log in, search, log out, follow, or exit",
    "You left the program, bye",
    "We don't have that option",
    "log out first before you create a new account",
    "Insert your name",
    "Insert your email",
    "Insert password",
    "Insert a valid email",
    "Sorry, that email is already taken",
    "log out first before you create a new acount",
    "Thank you for your registration, welcome!",
    "exit",
    "We don't have that account",
    "The password is incorrect",
    "You are already logged in",
    "Welcome, ",
    "Sorry, you have to be logged in to use that functionality",
    "insert the email of the person you want to follow",
    "That user does not exist",
    "You now follow ",
    "You logged out, see you later",
    "Insert the email of the person who you want to follow",
    "Followers: ",
    "Following: ",
    "We have no result for that query"
];


var exitCommand = "exit";
var validEmail;
usersList = [];
var alreadyLoggedIn = false;
var loggingInPerson;
var userFollowing;
var userThatWillBeFolowed;
var existentUsers = false;


function askUser() {
    var exit = false;
    while (!exit) {
        var commands = prompt(ask[0]);
        switch (commands) {
            case "log in":
                instagram.logIn();
                break;
            case "sign up":
                instagram.signUp();
                break;
            case "search":
                instagram.search();
                break;
            case "log out":
                instagram.logOut();
                break;
            case "follow":
                instagram.follow();
                break;
            case exitCommand:
                alert(ask[1]);
                exit = true;
                break;
            default:
                alert(ask[2] + "\n");
                break;
        }
    }
}

class Instagram {
    constructor(name, email, password, following, followers) {
        this.name = name;
        
        this.email = email;
        this.password = password;
        this.following = 0;
        this.followers = 0;
    }

    signUp() {
        if (alreadyLoggedIn == true) {
            alert(ask[3] + "\n");
            return;
        } else {
            var askname = prompt(ask[4]);
            var askEmail = prompt(ask[5]);

            var itIsValidEmail = this.validateEmail(askEmail);


            var emailAlreadyExists;

            for (let i = 0; i < usersList.length; i++) {
                if (usersList[i].email === askEmail) {
                    emailAlreadyExists = true;
                } else {
                    emailAlreadyExists = false;
                }
            }


            while (itIsValidEmail != true) {
                alert(ask[7] + "\n");
                var askEmail = prompt(ask[5]);
                var itIsValidEmail = this.validateEmail(askEmail);
                if (itIsValidEmail == true) {
                    var askPassword = prompt(ask[6]);

                    if (emailAlreadyExists == true) {
                        alert(ask[8] + "\n");
                        return;
                    }
                    else {
                        let utilizadores = new InstagramUser(askname, askEmail.trim(), askPassword);
                        usersList.push(utilizadores);
                        alert(ask[10] + "\n");
                        return;
                    }
                }
            }
            if (askEmail == exitCommand) {
                return;
            }


            if (itIsValidEmail == true) {
                var askPassword = prompt(ask[6]);

                if (emailAlreadyExists == true) {
                    alert(ask[8] + "\n");
                    return;
                }
                else {
                    let utilizadores = new InstagramUser(askname, askEmail.trim(), askPassword);
                    usersList.push(utilizadores);
                    alert(ask[10] + "\n");
                    return;
                }
            }
        }
    }


    validateEmail(askEmail) {
        if (askEmail.indexOf("@") < 0) {
            validEmail = false;
            return;
        }
        if (askEmail.indexOf(".") < 0) {
            validEmail = false;
            return;
        }

        var varUm = askEmail.substring(0, askEmail.indexOf("@"));
        var varDois = askEmail.substring(askEmail.indexOf("@") + 1, askEmail.indexOf(".", askEmail.indexOf("@")));
        var varTres = askEmail.substring(askEmail.indexOf(".", askEmail.indexOf("@")) + 1, askEmail.length);

        if (varUm.length == 0 || varDois.length == 0 || varTres.length == 0) {
            return false;
        }

        var space = " ";
        if (varUm.substring(1, varUm.length).match(space)) {
            return false;
        }
        if (varDois.substring(0, varUm.length).match(space)) {
            return false;
        }

        var exclamacao = "!";
        if (varUm.substring(0, varUm.length).match(exclamacao)) {
            return false;
        }
        if (varDois.substring(0, varUm.length).match(exclamacao)) {
            return false;
        }

        var letters = /^[0-9a-zA-Z\W_]+$/;
        if (varUm.match(letters) == null) {
            if (varUm.substring(0, varUm.length).match(letters) != null) {
            } else {
                return false;
            }
        }
        if (varDois.match(letters) == null) {
            return false;
        }
        var letters = /^[0-9a-zA-Z.]+$/;
        if (varTres.match(letters) == null) {
            if (varTres[varTres.length - 1] == " " && varTres.substring(0, varTres.length - 2).match(letters) != null) {
            } else {
                return false;
            }
        }

        if (varUm.indexOf("@") >= 0 || varDois.indexOf("@") >= 0 || varTres.indexOf("@") >= 0) {
            askEmail = false;
            return false;
        } else {
            askEmail.trim();
            askEmail = true;
            return true;
        }
    }


    logIn() {
        if (alreadyLoggedIn == true) {
            alert(ask[14] + "\n");
            return;
        }

        var acceptLogIn = false;

        var askEmail = prompt(ask[5]);
        var askPassword = prompt(ask[6]);

        for (let i = 0; i < usersList.length; i++) {
            if (usersList[i].email.trim() == askEmail.trim()) {
                acceptLogIn = true;
                loggingInPerson = usersList[i];
            }
        }

        if (acceptLogIn == false) {
            alert(ask[12] + "\n");
            return;
        }
        if (acceptLogIn == true && loggingInPerson.password != askPassword) {
            alert(ask[13] + "\n");
            return;
        }
        if (acceptLogIn == true && alreadyLoggedIn == false) {
            alert(ask[15] + loggingInPerson.name + "." + "\n");
            alreadyLoggedIn = true;
            return;
        }
    }

    logOut() {
        if (alreadyLoggedIn == false) {
            alert(ask[16] + "\n");
            return;
        }
        else if (alreadyLoggedIn == true) {
            alert(ask[20] + "\n");
            alreadyLoggedIn = false;
            return;
        }
    }

    follow() {
        var existentUsers = false;

        if (alreadyLoggedIn == false) {
            alert(ask[16] + "\n");
            return;
        }

        var askWhoWantsToFollow = prompt(ask[17] + "\n");

        for (let i = 0; i < usersList.length; i++) {
            if (usersList[i].email === askWhoWantsToFollow) {
                existentUsers = true;
                userThatWillBeFolowed = usersList[i];
            }
        }

        if (existentUsers == false) {
            alert(ask[18] + "\n");
            return;
        }
        for (let i = 0; i < usersList.length; i++) {
            if (existentUsers = true) {
                loggingInPerson.following = +1;
                userThatWillBeFolowed.followers = +1;
                alert(ask[19] + userThatWillBeFolowed.name + "\n");
                return;
            }
        }
        return;
    }

    search() {
        if (alreadyLoggedIn == false) {
            alert(ask[16] + "\n");
            return;
        }

        var askEmail = prompt(ask[21]);

        for (let i = 0; i < usersList.length; i++) {
            if (usersList[i].email == askEmail.trim()) {
                existentUsers = true;
                userThatWillBeFolowed = usersList[i];
            }
        }

        if (alreadyLoggedIn == true) {
            if (existentUsers == true) {
                alert(userThatWillBeFolowed.name + "\n" + userThatWillBeFolowed.email + "\n" + ask[22] + userThatWillBeFolowed.followers + "\n" + ask[23] + userThatWillBeFolowed.following + "\n")
            } else {
                alert(ask[24] + "\n")
            }
        }
        return;
    }
}

class InstagramUser extends Instagram { };
var instagram = new Instagram();


askUser();