var doSearch = function (array, targetValue) {
    var min = 0;
    var max = array.length - 1;
    var guess;
    var count = 1;

    while (min <= max) {
        guess = floor((max + min) / 2);
        println(guess);
        if (array[guess] === targetValue) {
            println(count);
            return guess;
        }
        else if (array[guess] < targetValue) {
            min = guess + 1;
        }
        else {
            max = guess - 1;
        }
        count++;
    }
    return -1;
};

var primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37,
    41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97];

var result = doSearch(primes, 73);
println("Found prime at index " + result);

Program.assertEqual(doSearch(primes, 73), 20);
Program.assertEqual(doSearch(primes, 71), 19);
Program.assertEqual(doSearch(primes, 2), 0);

// Big O for binary search algorithm is the efficiency and advantage of how many calculation are needed to find the result compared to linear search, comparing between the linear search and binary search, the binary search becomes much more efficient depending on how many cases we need to analyze as the elements grow.
//For example if we had to analyze a array with 2 million elements, in the worst case scenario the linear search would take 2 million calculations while the binary search would decrease this number to merely 21 calculations in the worst case scenario, by each time the calculation is done cutting in half the list of elements that needs to be evaluated, the matemathical representation of this would be log2 (n). 