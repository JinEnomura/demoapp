var fs = require('fs');

console.log("going to create a directory");
fs.writeFile('input.txt', 'Exercise 4 done', function(err){
    if (err) {
        return console.error(err);
    }

    console.log("data written successfully");
    console.log("Let's read newly written data");

    fs.readFile("input.txt", function(err, data){
        if(err){
            return console.error(err);
        }
        console.log("asynchronous read: " + data.toString());
    })
});