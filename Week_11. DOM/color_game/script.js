var boxes = document.getElementsByClassName('box');
var boxesArray = [boxes[0], boxes[1], boxes[2], boxes[3], boxes[4], boxes[5]];
var correctBox;
var theRGB = document.getElementById('rgb');
var resetButton = document.getElementsByClassName('reset');
var message = document.querySelector('.option-text');
var header = document.querySelector('#header');
var whole = document.querySelector('#whole');
var modes1 = document.querySelector('.modes1');
var modes2 = document.querySelector('.modes2');
var reset = document.querySelector('.reset')

// generates random color boxes and create 1 correct answer box

for (let i = 0; i < boxes.length; i++) {
    boxes[i].style.backgroundColor = "rgb(" + Math.floor(Math.random() * Math.floor(255)) + "," + Math.floor(Math.random() * Math.floor(255)) + "," + Math.floor(Math.random() * Math.floor(255)) + ")";

    correctBox = boxesArray[Math.floor(Math.random() * boxesArray.length)];
}

function createRandomColors(num) {
    var colorBoxes = [];
    for (let i = 0; i < num; i++) {
        colorBoxes.push(randomColor());
    }
    return colorBoxes
}

// outputs correct box rgb on page title
theRGB.textContent = correctBox.style.backgroundColor;

// if else color game
for (let i = 0; i < boxes.length; i++) {
    boxes[i].addEventListener('click', function() {
        var clickedColor = this.style.backgroundColor;

        if(clickedColor === correctBox.style.backgroundColor) {
            message.textContent = 'correct';
            reset.textContent = 'Play Again?';
            header.style.backgroundColor = clickedColor;
            whole.style.backgroundColor = clickedColor;
            for (let i = 0; i < boxes.length; i++) {
                boxes[i].style.backgroundColor = clickedColor;
                boxes[i].style.display = 'block';
            }
        }
        else {
            this.style.backgroundColor = 'rgb(46, 46, 46)';
            message.textContent = 'Try Again';
        }
    })
}

//reset button
reset.addEventListener('click', function () {
    location.reload();
})

// easy mode
modes1.addEventListener('click', function() {
    boxesArray[3].style.display = 'none';
    boxesArray[4].style.display = 'none';
    boxesArray[5].style.display = 'none';
    modes1.classList.add('selected');
    modes2.classList.remove('selected');
    boxesArray.splice(3, 3);
})

//hard mode
modes2.addEventListener('click', function(){
    boxesArray[3].style.display = 'block';
    boxesArray[4].style.display = 'block';
    boxesArray[5].style.display = 'block';
    boxesArray.push(boxesArray[3]);
    boxesArray.push(boxesArray[4]);
    boxesArray.push(boxesArray[5]);
    modes2.classList.add('selected');
    modes1.classList.remove('selected');
    location.reload();
})

