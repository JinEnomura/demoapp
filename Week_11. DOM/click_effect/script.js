var img = document.getElementsByTagName('img');
img[0].style.display = 'block';
img[1].style.display = 'none';

var button = document.getElementById('btn');

button.addEventListener('click', runEvent);

function runEvent() {
    if (img[0].style.display === 'block') {
        img[0].style.display = 'none';
        img[1].style.display = 'block';
    } else {
        img[0].style.display = 'block';
        img[1].style.display = 'none';
    }
}
