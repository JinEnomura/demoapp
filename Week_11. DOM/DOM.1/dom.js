// EXAMINE THE DOCUMENTS OBJECT //

//console.dir(document); 
// mostra os documentos na consola

// console.log(document.domain);
// console.log(document.URL);
// console.log(document.title);
// //document.title = 123;
// console.log(document.doctype);
// console.log(document.head);
// console.log(document.body);
// console.log(document.all);
// console.log(document.all[10]);
// //document.all[10].textContent = "Hello";
// console.log(document.forms[0]);
// console.log(document.links);
//console.log(document.images);


/******************** */
// GET ELEMENT BY ID //

// //console.log(document.getElementById('header-title'));
// var headerTitle = document.getElementById('header-title');
// var header = document.getElementById('main-header');
// //console.log(headerTitle);
// headerTitle.textContent = "Hello";
// textContent mostra todo o elemento mesmo que tenha styling diferentes

// headerTitle.innerText = "Goodbye";
//innerText tem atençao a diferenças de styling e neste caso como no span tem display none nao mostra o 123
//console.log(headerTitle.textContent);
//headerTitle.innerHTML = '<h3>Hello</h3>';

//header.style.borderBottom = 'solid 3px #000'
//acede ao css com style e adiciona borderBottom ou ao headerTitle ou no bottom do header


/***************************** */
// GET ELEMENTS BY CLASS NAME //

// var items = document.getElementsByClassName('list-group-item');
// console.log(items);
// console.log(items[1]);
// items[1].textContent = "Hello 2";
// items[1].style.fontWeight= 'bold';
// items[1].style.backgroundColor = 'yellow';

// // gives error, para mudar o background de todos estes elementos iteramos com for loop como descrito em baixo
// //items.style.backgroundColor = '#f4f4f4';

// for (let i = 0; i < items.length; i++) {
//     items[i].style.backgroundColor = '#f4f4f4';
// }


/************************** */
// GET ELEMENT BY TAG NAME //

// var li = document.getElementsByTagName('li');
// console.log(li);
// console.log(li[1]);
// li[1].textContent = "Hello 2";
// li[1].style.fontWeight= 'bold';
// li[1].style.backgroundColor = 'yellow';

// // iteramos como anteriormente mas agora  a identifica los pelo tag "li"
// //items.style.backgroundColor = '#f4f4f4';

// for (let i = 0; i < li.length; i++) {
//     li[i].style.backgroundColor = '#f4f4f4';
// }


//****************//
// QUERY SELECTOR //

// var header = document.querySelector('#main-header');
// header.style.borderBottom = 'solid 4px #ccc';

// var input = document.querySelector('input');
// input.value = 'Hello World'

// var submit = document.querySelector('input[type="submit"]');
// submit.value = "SEND";

// var item = document.querySelector('.list-group-item:last-child');
// item.style.color = "red";

// var lastItem = document.querySelector('.list-group-item');
// lastItem.style.color = "blue";

// var secondItem = document.querySelector('.list-group-item:nth-child(2)');
// secondItem.style.color = "coral";


//****************//
// QUERY SELECTOR // 

// var titles = document.querySelectorAll('.title');

// console.log(titles);
// titles[0].textContent = "Hello";

// var odd = document.querySelectorAll('li:nth-child(odd)');
// var even = document.querySelectorAll('li:nth-child(even)');

// for (let i = 0; i < odd.length; i++) {
//     odd[i].style.backgroundColor = '#f4f4f4';
//     even[i].style.backgroundColor = '#ccc';
// }

/********************* */
// TRAVERSING THE DOM //

// var itemList = document.querySelector('#items');
// parentNode
// console.log(itemList.parentNode);
// itemList.parentNode.style.backgroundColor = '#f4f4f4';
// console.log(itemList.parentNode.parentNode.parentNode);

// parentElement
// parentNode
// console.log(itemList.parentElement);
// itemList.parentElement.style.backgroundColor = 'yellow';
// console.log(itemList.parentElement.parentElement.parentElement);

// childNodes
// console.log(itemList.childNodes);

// console.log(itemList.children);
// console.log(itemList.children[1]);
// itemList.children[1].style.backgroundColor = 'yellow';

// FirstChild
// console.log(itemList.fisrtChild);
// firstElementChild
// console.log(itemList.firstElementChild);
// itemList.firstElementChild.textContent = 'Hello 1';

// lastChild
// console.log(itemList.fisrtChild);
// firstElementChild
// console.log(itemList.lastElementChild);
// itemList.lastElementChild.textContent = 'Hello 4';

// nextSibling
// console.log(itemList.nextSibling);
// // nextElementSibling
// console.log(itemList.nectElementSibling);

// previousSibling
// console.log(itemList.previousSibling);
// // previousElementSibling
// console.log(itemList.previousElementSibling);
// itemList.previousElementSibling.style.color = 'green';


/************* */
// createElement

// create  div
// var newDiv = document.createElement('div');

// // add class
// newDiv.className = 'Hello';

// // add id
// newDiv.id = 'Hello1';

// // add atribute
// newDiv.setAttribute('title', 'Hello Div');

// // create text node
// var newDivText = document.createTextNode('Hello World');

// //add text to div
// newDiv.appendChild(newDivText);

// var container = document.querySelector('header .container');
// var h1 = document.querySelector('header h1');

// console.log(newDiv);

// newDiv.style.fontSize = '30px';

// container.insertBefore(newDiv, h1);


/********** */
// EVENTS //

// var button = document.getElementById('button').addEventListener('click', buttonClick);

// function buttonClick(e) {
//     // console.log('Button clicked');
//     // document.getElementById('header-title').textContent = 'changed';
//     // document.querySelector('#main').style.backgroundColor = '#f4f4f4';
//     //console.log(e);


//     // console.log(e.target);
//     // console.log(e.target.id);
//     // console.log(e.target.className);
//     // console.log(e.target.classList);
//     // var output = document.getElementById('output');
//     // output.innerHTML = '<h3>'+e.target.id+'<h3>';

//     // console.log(e.type);

//     // posicao do click do rato em relacao a browser
//     // console.log(e.clientX);
//     // console.log(e.clientY);

//     // posiçao do click do rato dentro deste bloco de elemento
//     // console.log(e.offsetX);
//     // console.log(e.offsetY);

//     // retorna true ou false se no evento do clicke do mouse esta a pressionar as teclas shift, control ou alt
//     // console.log(e.altKey);
//     // console.log(e.ctrlKey);
//     // console.log(e.shiftKey);

// }

var button = document.getElementById('button');
var box = document.getElementById('box');

// button.addEventListener('click', runEvent);
// button.addEventListener('dblclick', runEvent);
//button.addEventListener('mousedown', runEvent);
//button.addEventListener('mouseup', runEvent);

// diferença entre estes 2 sao q mouseenter e mouseleave funciona para div todo, enquanto que mouseover e mouseout serve para elementos interiores como neste case <h3>
// box.addEventListener('mouseenter', runEvent);
// box.addEventListener('mouseleave', runEvent);

// box.addEventListener('mouseover', runEvent);
// box.addEventListener('mouseout', runEvent);


//box.addEventListener('mousemove', runEvent);

// var itemInput = document.querySelector('input[type="text"]');
// var form = document.querySelector('form');
// var select = document.querySelector('select');

// itemInput.addEventListener('keydown', runEvent);
// itemInput.addEventListener('keyup', runEvent);
//itemInput.addEventListener('keypress', runEvent);

// itemInput.addEventListener('focus', runEvent);
// itemInput.addEventListener('blur', runEvent);

// itemInput.addEventListener('cut', runEvent);
// itemInput.addEventListener('paste', runEvent);

//itemInput.addEventListener('input', runEvent);

// select.addEventListener('change', runEvent);
// select.addEventListener('input', runEvent);

// form.addEventListener('submit', runEvent);

// function runEvent(e) {
//     e.preventDefault();
//     console.log('EVENT TYPE: '+e.type);
    
//     // console.log(e.target.value);
//     // console.log(e.target.value);
//     // document.getElementById('output').innerHTML = '<h3>'+e.target.value+'</h3>';

//     //mostra as posicoes do rato dentro da caixa
//     //output.innerHTML = '<h3>MouseX: '+e.offsetX+' </h3><h3>MouseY: '+e.offsetY+'</h3>';

//     // altera as cores do espectro rgb baseado nas posicoes x e y do rato
//     //box.style.backgroundColor = "rgb("+e.offsetX+","+e.offsetY+",40)";
//     // document.body.style.backgroundColor = "rgb("+e.offsetX+","+e.offsetY+",40)";
// }