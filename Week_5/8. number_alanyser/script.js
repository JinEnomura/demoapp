let ask = prompt("insert a sequence of numbers");

function isEven(n) {
    return n % 2 == 0;
 }
 
function isOdd(n) {
    return Math.abs(n % 2) == 1;
} 

var res = "";


for (let i = 0; i < ask.length; i++) {
    if (i < 1) {
        res+=(ask[0]);
    }
    if (isEven(ask[i]) && isEven(ask[i+1])) {
        res+=('-');
        res+=(ask[i+1]);
    }
    if (isOdd(ask[i]) && isOdd(ask[i+1])) {
        res+=('#');
        res+=(ask[i+1]);
    }
    if (isEven(ask[i]) && isOdd(ask[i+1])) {
        res+=(ask[i+1]);
    }
    if (isOdd(ask[i]) && isEven(ask[i+1])) {
        res+=(ask[i+1]);
    }
}


console.log(res);

