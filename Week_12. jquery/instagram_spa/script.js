const ask = [
    "input your option: Sign up, Log in, search, log out, follow, or exit", "You left the program, bye", "We don't have that option", "log out first before you create a new account", "Insert your input", "Insert your email", "Insert password", "Insert a valid email", "Sorry, that email is already taken", "log out first before you create a new acount", "Thank you for your registration, welcome!", "exit", "We don't have that account", "The password is incorrect", "You are already logged in",
    "Welcome, ", "Sorry, you have to be logged in to use that functionality", "insert the email of the person you want to follow", "That user does not exist", "You now follow ", "You logged out, see you later", "Insert the email of the person who you want to follow", "Followers: ", "Following: ", "We have no result for that query"
];

var exitCommand = "exit";
var validEmail;
usersList = [];
var alreadyLoggedIn = false;
var loggingInPerson;
var userFollowing;
var userThatWillBeFolowed;
var existentUsers = false;


$(document).ready(function () {

    // finalidade do botao submit se esta seleccionado sign up ou log in//
    var $signupRadio;
    var $loginRadio;
    initialize();

    function initialize() {
        $signupRadio = $('input[id="signup"]');
        $loginRadio = $('input[id="login"]');

        $signupRadio.change(function () {
            $('#button').off();
            $('#button').click(function () {
                instagram.signUp();
            });
        });

        $loginRadio.change(function () {
            $('#button').off();
            $('#button').click(function () {
                instagram.logIn();
                loggingInPerson.showProfile();
            });
        });
    }

    function signUpAdd() {
        document.getElementById('welcome').innerHTML += `<div id="userArea">
        <input type="text" id="signedEmail" placeholder="User's email"><br>
        <button id="functionalities" class="search">Search</button>
        <button id="functionalities" class="myProfile">My profile</button>
        <button id="functionalities" class="follow">Follow this person</button>
        <button id="functionalities" class="logout">Log out</button><br>
        <input type="text" id="photoURL" placeholder ="Photo URL"><br>
        <textarea name="" id="description" placeholder="Description"cols="22" rows="3"></textarea><br>
        <button id="publish">Publish</button><br>
        <div id="profileInfo">
        <p class="name"></p>
        <p class="followers"></p>
        <p class="following"></p>
        </div>
        <div id="posts"></div>
        </div>`;

        $('#posts').css('display', 'flex');

        $('.logout').off().click(function () {
            $('#userArea').remove();
            instagram.logOut();
        });

        $('.follow').off().click(function () {
            instagram.follow();
            instagram.actualizeUserInfo();
        });

        $('.search').off().click(function () {
            instagram.clearPreviousPosts();
            instagram.search();
        });

        $('#publish').click(function () {
            instagram.publishPost();
        });

        $('.myProfile').click(function () {
            instagram.clearPreviousPosts();
            loggingInPerson.showProfile();
        });
    }


    // main page animation//
    function signupRemove() {
        $('#logo').remove();
        $('#checkboxes').remove();
        $('#form').remove();
    }


    function signupAddAtLogout() {
        document.getElementById('instagram').innerHTML += `<div id="logo"></div>
        <div id="welcome">
            <div id="form">
                <h2>Email</h2><br>
                <input type="text" id="email" class="required"><br>
                <h2>Password</h2><br>
                <input type="text" id="password" class="required"><br>
                <div id="extraInfo">
                    <h2>Name</h2><br>
                    <input type="text" id="name" class="required"><br>
                    <h2>ID / Username</h2><br>
                    <input type="text" id="username" class="required"><br>
                </div>
                <button id="button" type=button>Submit</button>
            </div>
            
            <form id="checkboxes">
                Sign Up
                <input type="radio" name="option" id="signup">
                Log In
                <input type="radio" name="option" id="login">
            </form>
        </div>`

        initialize();

        $('#logo').css('marginTop', '0')
        $('#extraInfo').hide();

        $('#checkboxes').css('color', 'white');

        $('#signup').click(function () {
            $('#extraInfo').slideDown(1500, function () {});
        }).click(function () {
            $('#checkboxes').css.color = $('#checkboxes').css('color', 'grey');
        });

        $('#login').click(function () {
            $('#extraInfo').slideUp(1500, function () {});
            $('#checkboxes').css.color = $('#checkboxes').css('color', 'white');
        })
    }


    function changeBackground() {
        $("#welcome").css({
            'background-color': '#ffffff'
        });
        $("#welcome").css('background-image', 'none');
        $("#welcome").css('width', '80%');
        $("#welcome").css('border', 'none');
        $("#welcome").css('margin-top', '5px');
    }

    function changeUserArea() {
        $('#signedEmail').css('margin-bottom', '10px');
        $('#functionalities').css('margin-bottom', '10px');
        $('#photoURL').css('margin-bottom', '10px');
    }

    $('#extraInfo').hide();

    $('#checkboxes').css('color', 'white');

    $('#signup').click(function () {
        $('#extraInfo').slideDown(1500, function () {});
    }).click(function () {
        $('#checkboxes').css.color = $('#checkboxes').css('color', 'grey');
    });

    $('#login').click(function () {
        $('#extraInfo').slideUp(1500, function () {});
        $('#checkboxes').css.color = $('#checkboxes').css('color', 'white');
    })

    class Post {
        constructor(imageURL, description) {
            this.imageURL = imageURL;
            this.description = description;
        }

        showPost() {
            var inject = `<div id="singlePost"><div id="postImage"></div><p class="description"></p></div>`;
            var novo = $(inject).appendTo('#posts');
            novo.children('#postImage').css('background-image', 'url(' + this.imageURL + ')');
            novo.children('.description').text(this.description);
        }
    }

    class Instagram {
        constructor(name, email, password, id, following, followers, posts) {
            this.name = name;
            this.email = email;
            this.password = password;
            this.id = id;
            this.following = 0;
            this.followers = 0;
            this.posts = [];
        }

        signupValidation(emailAlreadyExists, askname, askEmail, askPassword, username) {
            if (emailAlreadyExists == true) {
                alert(ask[8] + "\n");
            } else {
                let utilizadores = new InstagramUser(askname, askEmail.trim(), askPassword, username);
                usersList.push(utilizadores);
                alert(ask[10] + "\n");
            }
        }

        signUp() {
            if (alreadyLoggedIn == true) {

                return;
            } else {
                let askEmail = $('#email').val();
                let askPassword = $('#password').val();
                let askname = $('#name').val();
                let username = $('#username').val();

                let itIsValidEmail = this.validateEmail(askEmail);

                let emailAlreadyExists = false;

                for (let i = 0; i < usersList.length; i++) {
                    if (usersList[i].email === askEmail) {
                        emailAlreadyExists = true;
                    }
                }


                if (itIsValidEmail != true) {
                    alert(ask[7]);
                    askEmail = $('#email').val();
                    itIsValidEmail = this.validateEmail(askEmail);
                    if (itIsValidEmail == true) {
                        this.signupValidation(emailAlreadyExists, askname, askEmail, askPassword, username);
                        return;
                    }
                }


                if (itIsValidEmail == true) {
                    this.signupValidation(emailAlreadyExists, askname, askEmail, askPassword, username);
                    return;
                }
            }
        }


        validateEmail(askEmail) {
            if (askEmail.indexOf("@") < 0) {
                validEmail = false;
                return;
            }
            if (askEmail.indexOf(".") < 0) {
                validEmail = false;
                return;
            }

            var varUm = askEmail.substring(0, askEmail.indexOf("@"));
            var varDois = askEmail.substring(askEmail.indexOf("@") + 1, askEmail.indexOf(".", askEmail.indexOf("@")));
            var varTres = askEmail.substring(askEmail.indexOf(".", askEmail.indexOf("@")) + 1, askEmail.length);

            if (varUm.length == 0 || varDois.length == 0 || varTres.length == 0) {
                return false;
            }

            var space = " ";
            if (varUm.substring(1, varUm.length).match(space)) {
                return false;
            }
            if (varDois.substring(0, varUm.length).match(space)) {
                return false;
            }

            var exclamacao = "!";
            if (varUm.substring(0, varUm.length).match(exclamacao)) {
                return false;
            }
            if (varDois.substring(0, varUm.length).match(exclamacao)) {
                return false;
            }

            var letters = /^[0-9a-zA-Z\W_]+$/;
            if (varUm.match(letters) == null) {
                if (varUm.substring(0, varUm.length).match(letters) != null) {} else {
                    return false;
                }
            }
            if (varDois.match(letters) == null) {
                return false;
            }
            var letters = /^[0-9a-zA-Z.]+$/;
            if (varTres.match(letters) == null) {
                if (varTres[varTres.length - 1] == " " && varTres.substring(0, varTres.length - 2).match(letters) != null) {} else {
                    return false;
                }
            }

            if (varUm.indexOf("@") >= 0 || varDois.indexOf("@") >= 0 || varTres.indexOf("@") >= 0) {
                askEmail = false;
                return false;
            } else {
                askEmail.trim();
                askEmail = true;
                return true;
            }
        }

        logIn() {
            if (alreadyLoggedIn == true) {
                alert(ask[14] + "\n");
                return;
            }

            var acceptLogIn = false;

            var askEmail = $('#email').val();
            var askPassword = $('#password').val();

            for (let i = 0; i < usersList.length; i++) {
                if (usersList[i].email.trim() == askEmail.trim()) {
                    acceptLogIn = true;
                    loggingInPerson = usersList[i];
                }
            }

            if (acceptLogIn == false) {
                alert(ask[12] + "\n");
                return;
            }
            if (acceptLogIn == true && loggingInPerson.password != askPassword) {
                alert(ask[13] + "\n");
                return;
            }
            if (acceptLogIn == true && alreadyLoggedIn == false) {
                alert(ask[15] + loggingInPerson.name + "." + "\n");

                signUpAdd();
                signupRemove();
                changeBackground();
                changeUserArea();

                alreadyLoggedIn = true;
                return;
            }
        }

        logOut() {
            alert(ask[20] + "\n");
            alreadyLoggedIn = false;
            signupAddAtLogout();
        }


        follow() {
            var existentUsers = false;

            if (alreadyLoggedIn == false) {
                alert(ask[16] + "\n");
                return;
            }

            var askWhoWantsToFollow = $('#signedEmail').val();

            for (let i = 0; i < usersList.length; i++) {
                if (usersList[i].email === askWhoWantsToFollow) {
                    existentUsers = true;
                    userThatWillBeFolowed = usersList[i];
                }
            }

            if (existentUsers == false) {
                alert(ask[18] + "\n");
                return;
            }
            for (let i = 0; i < usersList.length; i++) {
                if (existentUsers = true) {
                    loggingInPerson.following = +1;
                    userThatWillBeFolowed.followers = +1;
                    alert(ask[19] + userThatWillBeFolowed.name + "\n");
                    return;
                }
            }
            return;
        }

        search() {
            if (alreadyLoggedIn == false) {
                alert(ask[16] + "\n");
                return;
            }

            var askEmail = $('#signedEmail').val();

            for (let i = 0; i < usersList.length; i++) {
                if (usersList[i].email == askEmail.trim()) {
                    existentUsers = true;
                    userThatWillBeFolowed = usersList[i];
                }
            }

            if (alreadyLoggedIn == true) {
                if (existentUsers == true) {
                    userThatWillBeFolowed.showProfile();
                } else {
                    alert(ask[24] + "\n")
                }
            }
            return;
        }

        clearPreviousPosts() {
            $('#posts').children('#singlePost').remove();
        }

        publishPost() {
            let imageURL = $('#photoURL').val().slice();
            let description = $('#description').val().slice();

            let postNovo = new Post(imageURL, description);
            loggingInPerson.posts.push(postNovo);

            postNovo.showPost();
        }

        actualizeUserInfo() {
            $('.name').text("User : " + this.name);
            $('.following').text("Following : " + this.following);
            $('.followers').text("Followers : " + this.followers);
        }

        showProfile() {
            this.actualizeUserInfo();
            var len = this.posts.length;
            for (let i = 0; i < len; i++) {
                this.posts[i].showPost();
            }
        }
    }

    class InstagramUser extends Instagram {};
    var instagram = new InstagramUser();

});